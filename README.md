# ChatterBOT - UNAC

_Proyecto desarrollado por un alumno de la unac para fines estudiantiles y de aprendizaje sobre inteligencia artificial y redes neuronales_

## Comenzando 🚀

_ Esto es un ejemplo preconfigurado para usar la Libreria de Chatterbot_.
Usando el Framework Django_ y como entorno virtual **Docker** y **Docker-Compose** .
app using **Django** and **ChatterBot** _

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Para poder correr en local el Chatbot necesitas todo esto e instalar el requirements.txt_

```
- Docker
- Docker Compose
- Django-Rest-Framework
- Python 3.0
- Chatterbot
- Chatterbot-corpus
```

### Instalación 🔧

_Luego de instalar **Docker** y **Docker-Compose** se procede a ejecutar los siguientes comandos desde el terminal Linux_
```
docker-compose build o docker-compose -d
```
_Para comenzar una nueva App_
```
docker-compose exec web /bin/sh -c "cd ./src/apps  && python ../manage.py startapp user"
```
_Migrate
```
docker-compose exec web /bin/sh -c "python src/manage.py migrate"
```
_Create SuperUser
```
docker-compose exec web /bin/sh -c "python src/manage.py createsuperuser"
```
_Y para lanzar el proyecto tienes que colocar _
```
docker-compose up
```
_Luego colocar en tu navegador
```
http://0.0.0.0:8000/
```
## Ejecutando las pruebas ⚙️
_Explica como ejecutar las pruebas automatizadas para este sistema_

### Analice las pruebas end-to-end 🔩

_Comenzamos a Trainear al Chatbot_

```
    trainer = ListTrainer(chatterbot)
    trainer.train([
        'hola robot',
        'hola un gusto volverte a ver',

    ])
```

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Docs](https://chatterbot.readthedocs.io/en/stable/tutorial.html)

## Versionado 📌

Usamos GitLab para el control de versiones, actualmente el repositorio cuenta con una Rama Master y Issues creadas por el usuario.
Pasan una revision antes de ser integradas en la rama Master.

## Autores ✒️

_Diego De la Vega Loayza_

* **Gunther Cox** - *Trabajo Inicial* - [Gunther Cox](https://github.com/gunthercox/ChatterBot)

## Licencia 📄

Este proyecto no tiene licencia, es de codigo abierto para que pueda ser modificado

## Expresiones de Gratitud 🎁

* Un Agradecimiento a mis Padres por aguantar las teclas de la pc, a altas horas de la madrugada 📢
* una 🍺 y un ☕ a @HarolCalzada por su apoyo con la configuracion del Entorno **Docker**. 
* Gracias a todos por utilizar este Repositorio 🤓.


---
⌨️ con ❤️ por [DDelavega](https://gitlab.com/ddvloayza) 😊